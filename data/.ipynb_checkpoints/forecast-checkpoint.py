from __future__ import division, print_function, unicode_literals

import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns
import statsmodels.api as sm
from sklearn import preprocessing

import utils
from keymap import *


class SARIMAX():
    def __init__(self, train, aggregation='D', seasonality=False,
                 order=(1, 0, 0), seasonal_order=(0, 0, 0, 0)):
        """Initializes SARIMAX Model.

        Parameters
        ----------
        train : pandas.Series
            Pandas series containing the training set, with dates as
            the indices.

        aggregation : string
            String to set aggregation of dataset. Can be 'D' for days, 'W' for
            weeks, 'M' for monthly, 'Q' for quarterly, and 'Y' for yearly.

        seasonality : boolean, int
            Seasonal period of the dataset. If `False`, data is non-seasonal.
            Otherwise, an integer count of how many data points correspond to a
            season.

        Returns
        -------
        """
        self.train = train
        if seasonality:
            self.period = seasonality
        else:
            self.period = utils.periodicity[aggregation]
        self.seasonal_order = seasonal_order or (1, 1, 0, self.period)
        self.order = order
        self.scaler = preprocessing.MinMaxScaler()
        self.params = None
        self.model(order=self.order, seasonal_order=self.seasonal_order)

    def scaling(self, data):
        """Rescales dataset using `self.scaler`.

        Will attempt to rescale data using `self.scaler.transform`. If data is
        not fitted yet, will use `self.scaler.fit_transform` instead.

        Parameters
        ----------
        data: array-like
            Data to be rescaled

        Returns
        -------
        array-like
            The standard scaled form of `data`.

        """
        try:
            values = data.values
        except AttributeError:
            values = data
        if values.ndim==1:
            values = values.reshape(-1,1)
        try:
            vals = self.scaler.transform(values)
        except AttributeError:
            vals = self.scaler.fit_transform(values)
        return pd.Series(data=vals.ravel(), index=data.index)

    def scaling_inverse(self, data):
        """Inverse operation of `scaling`.

        Parameters
        ----------
        data: array-like
            Data to be rescaled

        Returns
        -------
        array-like
            The unscaled form of `data`.

        """
        try:
            values = data.values
        except AttributeError:
            values = data
        if values.ndim==1:
            values = values.reshape(-1,1)
        vals = self.scaler.inverse_transform(values)
        return pd.Series(data=vals.ravel(), index=data.index)

    def model(self, order=(0, 1, 1), seasonal_order=(1, 1, 0, 52)):
        self.mod = sm.tsa.statespace.SARIMAX(
                                self.scaling(self.train),
                                order=order,
                                seasonal_order=seasonal_order,
                                enforce_stationarity=False,
                                enforce_invertibility=False)

    def fit(self, init_params=None, disp=0):
        self.fitted = self.mod.fit(init_params, disp=disp)
        self.params = self.fitted.params
        return self.params

    def get_forecast(self, plot=False, ci=True, ax=None, label='Forecast'):
        pred_uc = self.fitted.get_forecast(steps=self.period*2)
        pred_ci = pred_uc.conf_int().apply(lambda x: 
                        self.scaling_inverse(x), axis=0).values
        dates = pd.date_range(start=self.train.index[-1],
                              periods=self.period*2+1,
                              freq=self.train.index.freq)[1:]
        pred_mean = self.scaling_inverse(pred_uc.predicted_mean).ravel()
        if plot:
            if ax is None:
                fig = plt.figure()
                ax = fig.add_subplot(111)
            ax.plot(dates, pred_mean, label=label)
            if ci:
                ax.fill_between(dates,
                                pred_ci[:, 0],
                                pred_ci[:, 1], color='k', alpha=.25)
            ax.set_xlabel('Date')
            ax.set_ylabel('Sales')
            ax.legend()
            sns.despine()
        self.forecast = {"dates": dates, "pred_mean": pred_mean, "pred_ci": pred_ci}
        return self.forecast

    def benchmark(self):
        validation_date = self.train.index[-self.period]
        pred = self.fitted.get_prediction(
                    start=pd.to_datetime(validation_date), 
                    dynamic=True)
        y_forecasted = pred.predicted_mean
        y_truth = self.scaling(self.train.loc[validation_date:])
        mse = self.mse(y_forecasted, y_truth)
        me = self.me(y_forecasted, y_truth)
        return mse, me

    def mse(self, y_forecasted, y_truth):
        mse = ((y_forecasted - y_truth) ** 2).mean()
        return mse

    def me(self, y_forecasted, y_truth):
        me = (y_forecasted - y_truth).mean()
        return me
